# xLearning

Welcome to my xLearning journey at xHub! My own projects and notes are organized in this repository as I learn about various technologies and topics.

## Structure

For this journey, I am following a personal Git branching model that involves learning branches. The repository is structured so that a folder is created for each topic I want to learn.

<p align="center">
<a href="https://i.ibb.co/SwzQ7cV/Gitlearning-by-Hassan.png"><img src="https://i.ibb.co/SwzQ7cV/Gitlearning-by-Hassan.png" alt="Gitlearning"></a>
</p>

| Branch                        | Purpose                                                         | Example                                                                     |
| ----------------------------- | --------------------------------------------------------------- | --------------------------------------------------------------------------- |
| `main`                        | The main branch for production-ready code                       | Contains the latest stable release of the project                           |
| `hotfix`                      | A branch for emergency fixes to production code                 | Fixing a critical security vulnerability on the live website                |
| `develop`                     | The branch for ongoing development work                         | Includes new features and changes that are being developed                  |
| `learning-{TOPIC}`            | Branches for learning and experimenting with a particular topic | `learning-java` for learning Java programming language                      |
| `learning-{TOPIC}-{SUBTOPIC}` | Sub-branches for further exploration of a particular topic      | `learning-java-basics` for focusing on the fundamentals of Java programming |

## Usage

Each folder contains its own projects and notes on the topic. Feel free to explore them.

Please note that the projects and notes in this repository are for my personal learning purposes only and not for commercial or public use.

## Contact

If you have any questions or suggestions, please feel free to contact me.
